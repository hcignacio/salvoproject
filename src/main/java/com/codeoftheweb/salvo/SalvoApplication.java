package com.codeoftheweb.salvo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SalvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

/*
	@Bean
	public CommandLineRunner initData(PlayerRepository repository) {
		return (args) -> {
			// save a couple of customers
			repository.save(new Player("Jack", "Bauer"));
			repository.save(new Player("Chloe", "O'Brian"));
			repository.save(new Player("Kim", "Bauer"));
			repository.save(new Player("David", "Palmer"));
			repository.save(new Player("Michelle", "Dessler"));
		};
	}


	@Bean
	public CommandLineRunner initDataGame(GameRepository repository){
		return (args) -> {

			Date firstDate = new Date();
			Date secondDate = Date.from(firstDate.toInstant().plusSeconds(3600));
			Date thirdDate = Date.from(secondDate.toInstant().plusSeconds(3600));
			repository.save(new Game(firstDate));
			repository.save(new Game(secondDate));
			repository.save(new Game(thirdDate));
		};
	}

*/

	@Bean
	public CommandLineRunner initDataGamePlayer(PlayerRepository playerRepository,
												GameRepository gameRepository,
												GamePlayerRepository gamePlayerRepository,
												ShipRepository shipRepository,
												SalvoRepository salvoRepository,
												ScoreRepository scoreRepository)
	{
		return (String... args) -> {

			// PLAYERS
			Player player1 = new Player("player1@gmail.com","pass1");
			Player player2 = new Player("player2@gmail.com","pass2");
			Player player3 = new Player("player3@gmail.com","pass3");
			Player player4 = new Player("player4@gmail.com","pass4");
			Player player5 = new Player("player5@gmail.com","pass5");
			Player player6 = new Player("player6@gmail.com","pass6");

			//GAMES
			Game game1 = new Game();
			Game game2 = new Game();
			Game game3 = new Game();

			//GAMEPLAYERS
			GamePlayer gamePlayer1 = new GamePlayer(player1, game1);
			GamePlayer gamePlayer2 = new GamePlayer(player2, game2);
			GamePlayer gamePlayer3 = new GamePlayer(player3, game3);
			GamePlayer gamePlayer4 = new GamePlayer(player4, game1);
			GamePlayer gamePlayer5 = new GamePlayer(player5, game2);
			GamePlayer gamePlayer6 = new GamePlayer(player6, game3);

			playerRepository.save(player1);
			playerRepository.save(player2);
			playerRepository.save(player3);
			playerRepository.save(player4);
			playerRepository.save(player5);
			playerRepository.save(player6);

			gameRepository.save(game1);
			gameRepository.save(game2);
			gameRepository.save(game3);

			gamePlayerRepository.save(gamePlayer1);
			gamePlayerRepository.save(gamePlayer2);
			gamePlayerRepository.save(gamePlayer3);
			gamePlayerRepository.save(gamePlayer4);
			gamePlayerRepository.save(gamePlayer5);
			gamePlayerRepository.save(gamePlayer6);

			//SHIPS´ LOCATIONS
			List<String> location1 = new ArrayList<>();
			location1.add("A1");
			location1.add("A2");
			location1.add("A3");
			List<String> location2 = new ArrayList<>();
			location2.add("C3");
			location2.add("C4");
			List<String> location3 = new ArrayList<>();
			location3.add("A5");

			//SHIPS
			Ship ship1 = new Ship("Destroyer", location1, gamePlayer1);
			shipRepository.save(ship1);
			Ship ship2 = new Ship("Submarine", location2, gamePlayer2);
			shipRepository.save(ship2);
			Ship ship3 = new Ship("Patrol Boat", location3, gamePlayer3);
			shipRepository.save(ship3);
			Ship ship4 = new Ship("Destroyer", location3, gamePlayer4);
			shipRepository.save(ship4);
			Ship ship5 = new Ship("Submarine", location2, gamePlayer5);
			shipRepository.save(ship5);
			Ship ship6 = new Ship("Patrol Boat", location1, gamePlayer6);
			shipRepository.save(ship6);

			//SALVOES´ LOCATIONS
			List<String> shoot1 = new ArrayList<>();
			shoot1.add("F1");
			List<String> shoot2 = new ArrayList<>();
			shoot2.add("A2");
			List<String> shoot3 = new ArrayList<>();
			shoot3.add("D5");
			List<String> shoot4 = new ArrayList<>();
			shoot4.add("H8");
			List<String> shoot5 = new ArrayList<>();
			shoot5.add("J6");
			List<String> shoot6 = new ArrayList<>();
			shoot6.add("I10");

			//TURNS
			int turn1 = 1;

			//SALVOES
			Salvo salvo1 = new Salvo(turn1, shoot1,gamePlayer1);
			Salvo salvo2 = new Salvo(turn1, shoot2, gamePlayer4);
			Salvo salvo3 = new Salvo(turn1, shoot3, gamePlayer2);
			Salvo salvo4 = new Salvo(turn1, shoot4,gamePlayer5);
			Salvo salvo5 = new Salvo(turn1, shoot5, gamePlayer3);
			Salvo salvo6 = new Salvo(turn1, shoot6, gamePlayer6);

			salvoRepository.save(salvo1);
			salvoRepository.save(salvo2);
			salvoRepository.save(salvo3);
			salvoRepository.save(salvo4);
			salvoRepository.save(salvo5);
			salvoRepository.save(salvo6);

			//SCORES
			Score score1 = new Score(player1, game1, 1.0);
			Score score2 = new Score(player4, game1, 0.0);
			Score score3 = new Score(player2, game2, 0.0);
			Score score4 = new Score(player5, game2, 1.0);
			Score score5 = new Score(player3, game3, 0.5);
			Score score6 = new Score(player6, game3, 0.5);

			scoreRepository.save(score1);
			scoreRepository.save(score2);
			scoreRepository.save(score3);
			scoreRepository.save(score4);
			scoreRepository.save(score5);
			scoreRepository.save(score6);
		};
	}

}
