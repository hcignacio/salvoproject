package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
public class GamePlayer {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private Date joinDate;

    @ManyToOne(fetch = FetchType.EAGER)
    //Establecemos la relacion con player (.eager, trae toda la informacion al mismo tiempo)
    @JoinColumn(name = "player_id")         //Nombramos la columna
    private Player playerPlay;

    @ManyToOne(fetch = FetchType.EAGER)
    //Establecemos la relacion con game (.eager, trae toda la informacion al mismo tiempo)
    @JoinColumn(name = "game_id")           //Nombramos la columna
    private Game gamePlay;

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Ship> shipPlay = new ArrayList<>();

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Salvo> salvoPlay = new ArrayList<>();

    public GamePlayer() {
    }

    public GamePlayer(Player player, Game game) {
        this.joinDate = game.getCreationDate();
        this.playerPlay = player;
        this.gamePlay = game;
    }

    /*
    public GamePlayer(Date joinDate, Player player, Game game){
        // this.id = id;
        this.joinDate = new Date();
        this.playerPlay = player;
        this.gamePlay = game;
    }
    */

    public long getId() {
        return id;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    @JsonIgnore
    public Player getPlayer() {
        return playerPlay;
    }

    @JsonIgnore
    public Game getGame() {
        return gamePlay;
    }

    @JsonIgnore
    public List<Ship> getShips() {
        return shipPlay;
    }

    @JsonIgnore
    public List<Salvo> getSalvoes() {
        return salvoPlay;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public void setPlayerPlay(Player playerPlay) {
        this.playerPlay = playerPlay;
    }

    public void setGamePlay(Game gamePlay) {
        this.gamePlay = gamePlay;
    }

    public void setShipPlay(List<Ship> shipPlay) {
        this.shipPlay = shipPlay;
    }

    public void addShip(Ship ship) {
        ship.setGamePlayer(this);
        this.shipPlay.add(ship);
    }

    public void addSalvo(Salvo salvo) {
        salvo.setGamePlayer(this);
        this.salvoPlay.add(salvo);
    }

}

