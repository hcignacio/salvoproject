package com.codeoftheweb.salvo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SalvoJustController {

    private GameRepository gameViewRepository;

    @RequestMapping("/")
    public String getGamesView(Model model) {
        //model.addAttribute("gp", );
        return "redirect:/web/game.html";
    }

}
