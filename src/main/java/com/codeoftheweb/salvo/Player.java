package com.codeoftheweb.salvo;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String email;

    private String password;

    @OneToMany(mappedBy = "playerPlay", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<GamePlayer> gamePlayers = new ArrayList<>();

    @OneToMany(mappedBy = "playerScore", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Score> scores = new ArrayList<>();

    public Player() {}

    public Player(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public List<GamePlayer> getGamePlayers(){ return gamePlayers; }

    public void addPlayer(GamePlayer gamePlayer){
        gamePlayer.setPlayerPlay(this);
        gamePlayers.add(gamePlayer);
    }

    public void addPlayerScore(Score score){
        score.setPlayerScore(this);
        scores.add(score);
    }

    @JsonIgnore
    public List<Score> getScores(){
        return scores;
    }

    @JsonIgnore
    public Optional<Score> getScore(Game game){
        //List<GamePlayer> gamePlayer = game.getGamePlayers();
        //List<Score> scores = this.getScores();
        return this.getScores()
                .stream()
                .filter(score -> score.getGameScore().equals(game))
                .findFirst();
        //.filter(score -> score.getScore().equals(game.getScores()))

        //.filter(score -> score.getGameScore().equals(game))
    }

    /*
    public Double getTotalScore(){

    }

    public int getWins(){
        return                 this.getScores()
                .stream()
                .filter(score -> score.getScore() == 1);
    }
    */

    /*
    public Score getScore(Game game){
        long gameId = game.getId();
        Score score = this.scores.stream().findFirst(gameId);
        return score;
    }



    public Optional<Score> getScore(){
        Score score = new Score();
        return score
                .stream()
                .filter(s -> s.getName().findFirst().orElse(null);
    }

    */

}
