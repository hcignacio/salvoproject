package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    private GameRepository gameListedRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @RequestMapping(value = "/game_view/{game_id}", method = RequestMethod.GET)                // getGamesView
    public Map<String, Object> getGamesView(@PathVariable("game_id") long game_id) {

        GamePlayer gamePlayer = gamePlayerRepository.getOne(game_id);
        return getGamesViewToDTO(gamePlayer.getGame(), gamePlayer);
    }

    private Map<String, Object> getGamesViewToDTO(Game game, GamePlayer gamePlayer) {           // getGamesViewToDTO
        Map<String, Object> dtoGame = new LinkedHashMap<String, Object>();

        dtoGame.put("id", game.getId());
        dtoGame.put("creationDate", game.getCreationDate());
        dtoGame.put("gamePlayers", gamePlayerDTO(game.getGamePlayers()));
        dtoGame.put("ships", getLocationShips(gamePlayer.getShips()));
        dtoGame.put("salvoes", getLocationSalvoes(game));
        return dtoGame;
    }

    private List<Map<String, Object>> getLocationSalvoes(Game game){
        List<Map<String, Object>> salvoesList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> salvoesList.addAll(SalvoLocationsList(gamePlayer.getSalvoes())));
        return salvoesList;
    }

    private List<Map<String, Object>> SalvoLocationsList(List<Salvo> salvoList){
        return salvoList.stream()
                .map(salvo -> SalvoDTO(salvo))
                .collect(Collectors.toList());
    }

    private Map<String, Object> SalvoDTO(Salvo salvo) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("locations", salvo.getSalvoLocations());
        return dto;
    }

    private List<Map<String, Object>> gamePlayerDTO(List<GamePlayer> gamePlayers) {
        return gamePlayers.stream()
                .map(gamePlayer -> gamePlayerList(gamePlayer))
                .collect(Collectors.toList());
    }



    private List<Map<String, Object>> getLocationShips(List<Ship> ships){
        return ships.stream()
                .map(ship -> shipsDTO(ship))
                .collect(Collectors.toList());
    }

    private Map<String, Object> shipsDTO(Ship ship){
        Map<String, Object> dtoShip = new LinkedHashMap<String, Object>();
        dtoShip.put("shipType", ship.getShipType());
        dtoShip.put("locations", ship.getShipLocation());
        return dtoShip;
    }

    @RequestMapping("/ships_view")
    public List<Map<String, Object>> getShips(){
        return shipRepository.findAll()
                .stream()
                .map(ship -> shipsDTO(ship))
                .collect(Collectors.toList());
    }


    @RequestMapping("/games")
    public List<Object> getGames() {
        return gameListedRepository
                .findAll()
                .stream()
                .map(game -> gameDTO(game))
                .collect(Collectors.toList());
    }

    private Map<String, Object> gameDTO(Game game) {
        Map<String, Object> dto = new LinkedHashMap<>();

        dto.put("id", game.getId());
        dto.put("creationDate", game.getCreationDate());
        dto.put("gamePlayers", gamePlayerDTO(game.getGamePlayers()));
        //dto.put("players", getPlayers(game.getGamePlayers());
        //dto.put("scores" ,getScore(game));

        return dto;
    }

    private List<Map<String, Object>> getPlayers(List<Player> players){
        return players.stream()
                .map(player -> playerDTO(player))
                .collect(Collectors.toList());
    }

    private Map<String, Object> playerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("email", player.getEmail());

        return dto;
    }

    /*
    @RequestMapping("/games")
    public List<Object> getGames() {
        return gamePlayerRepository
                .findAll()
                .stream()
                .map(gp -> gamePlayerList(gp))
                .collect(Collectors.toList());
    }
    */

    private Map<String, Object> gamePlayerList(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getId());
        dto.put("player", gamePlayer.getPlayer());
        dto.put("joinDate", gamePlayer.getJoinDate());
        dto.put("scores", gamePlayer.getPlayer().getScore(gamePlayer.getGame()));
        //dto.put("scores", scoreDTO(gamePlayer));
        //dto.put("scores", scoreDTO(gamePlayer.getPlayer()));
        //dto.put("scores", getScores(gamePlayer));
        //dto.put("score", gamePlayer.getPlayer().getScore(gamePlayer.getGame()));
        //dto.put("score", gamePlayer.getPlayer().getScores());

        return dto;
    }

    /*
    private List<Map<String, Object>> getScores(GamePlayer gamePlayer) {
        return gamePlayer.stream()
                .map(ship -> shipsDTO(ship))
                .collect(Collectors.toList());
    }
    */

    /*
    private Map<String, Object> scoreDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerID", gamePlayer.getPlayer());
        dto.put("score", gamePlayer.getPlayer().getScore(gamePlayer.getGame()));
        //dto.put("finishDate", gamePlayer.getPlayer().getScore().f)

        return dto;
    }
    */


    /*
    private Map<String, Object> scoreDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerID", gamePlayer.getPlayer());
        dto.put("score", gamePlayer.getPlayer().getScore(gamePlayer.getGame()));


        return dto;
    }
    */

    /*
    private Map<String, Object> scoreDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("score", player.getScores());

        return dto;
    }
    */


    /*
    private Optional<Score> getScore(List<Game> games){
        return games.stream()
                .map(game -> scoresDTO(game))
                .collect(Collectors.toList());
    }

    private Map<String, Object> scoresDTO(Game game){
        Map<String, Object> dtoScores = new LinkedHashMap<String, Object>();
        dtoScores.put("score", game.ge());
        return dtoScores;
    }
    */

}