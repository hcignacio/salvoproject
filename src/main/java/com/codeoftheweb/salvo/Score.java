package com.codeoftheweb.salvo;

import javax.persistence.Entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Score {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)     //Establecemos la relacion con player (.eager, trae toda la informacion al mismo tiempo)
    @JoinColumn(name = "player_id")         //Nombramos la columna
    private Player playerScore;

    @ManyToOne(fetch = FetchType.EAGER)     //Establecemos la relacion con game (.eager, trae toda la informacion al mismo tiempo)
    @JoinColumn(name = "game_id")           //Nombramos la columna
    private Game gameScore;

    private Double score;

    private Date finishDate;

    public Score() {
        this.finishDate = new Date();
    }

    public Score(Player playerScore, Game gameScore, Double score) {
        this.playerScore = playerScore;
        this.gameScore = gameScore;
        this.score = score;
        this.finishDate = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public Player getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(Player playerScore) {
        this.playerScore = playerScore;
    }

    @JsonIgnore
    public Game getGameScore() {
        return gameScore;
    }

    public void setGameScore(Game gameScore) {
        this.gameScore = gameScore;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Player getPlayerId(){
        return this.getPlayerScore();
    }
}
